# frozen_string_literal: true

require 'zlib'

# The percent rollout strategy is the most comprehensive included with Gitlab::Experiment. It allows specifying the
# percentages per variant using an array, a hash, or will default to even distribution when no rules are provided.
#
# A given experiment id (context key) will always be given the same variant assignment.
#
# class ExampleExperiment < ApplicationExperiment
#   default_rollout :percent # even distribution between red and blue
#   default_rollout :percent, # red = 60, blue = 40
#                   distribution: [60, 40]
#   default_rollout :percent, # red = 30, blue = 70
#                   distribution: { red: 30, blue: 70 }
#
#   def control_behavior; end
#   def red_behavior; end
#   def blue_behavior; end
# end
#
module Gitlab
  class Experiment
    module Rollout
      class Percent < Base
        def execute
          crc = normalized_id
          total = 0

          case distribution_rules
          when Array # run through the rules until finding an acceptable one
            variant_names[distribution_rules.find_index { |percent| crc % 100 <= total += percent }]
          when Hash # run through the variant names until finding an acceptable one
            distribution_rules.find { |_, percent| crc % 100 <= total += percent }.first
          else variant_names[crc % variant_names.length] # assume even distribution on no rules
          end
        end

        def validate!
          case distribution_rules
          when nil then nil
          when Array, Hash
            if distribution_rules.length != variant_names.length
              raise InvalidRolloutRules, "the distribution rules don't match the number of variants defined"
            end
          else
            raise InvalidRolloutRules, 'unknown distribution options type'
          end
        end

        private

        def normalized_id
          Zlib.crc32(id, nil)
        end

        def distribution_rules
          @options[:distribution]
        end
      end
    end
  end
end
