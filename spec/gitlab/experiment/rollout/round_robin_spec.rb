# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Rollout::RoundRobin, :cache do
  subject(:subject_experiment) { Gitlab::Experiment.new(:example) }

  before do
    stub_experiments(example: true)
    subject_experiment.rollout(described_class)
  end

  it "cycles through the provided variants" do
    subject_experiment.variant(:variant1) {}
    subject_experiment.variant(:variant2) {}
    subject_experiment.variant(:variant3) {}

    expect(subject_experiment.assigned.name).to eq('variant1')

    subject_experiment.instance_variable_set(:@_assigned_variant_name, nil)
    subject_experiment.cache.delete

    expect(subject_experiment.assigned.name).to eq('variant2')

    subject_experiment.instance_variable_set(:@_assigned_variant_name, nil)
    subject_experiment.cache.delete

    expect(subject_experiment.assigned.name).to eq('variant3')

    subject_experiment.instance_variable_set(:@_assigned_variant_name, nil)
    subject_experiment.cache.delete

    expect(subject_experiment.assigned.name).to eq('variant1')
  end
end
