# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Callbacks do
  subject(:subject_experiment) { Gitlab::Experiment.new(:example) }

  after do
    subject_experiment.class.reset_callbacks(:exclusion_check)
    subject_experiment.class.reset_callbacks(:segmentation)
    subject_experiment.class.reset_callbacks(:segmentation_skipped)
    # subject_experiment.class.reset_callbacks(:run) # no easy way to do this
    subject_experiment.class.reset_callbacks(:run_skipped)
  end

  it "defines the expected callbacks" do
    expect(subject_experiment.class.__callbacks).to include(:exclusion_check)
    expect(subject_experiment.class.__callbacks).to include(:segmentation)
    expect(subject_experiment.class.__callbacks).to include(:segmentation_skipped)
    expect(subject_experiment.class.__callbacks).to include(:run)
    expect(subject_experiment.class.__callbacks).to include(:run_skipped)
  end

  describe ".exclude" do
    it "adds the filters to the correct callback chain" do
      subject_experiment.class.exclude { false }
      subject_experiment.class.exclude { true }
      subject_experiment.class.exclude { raise(:not_expected) } # this will never get called because we abort

      expect(subject_experiment.run_callbacks(:exclusion_check)).to be_falsey
    end

    it "raises an exception when there are no filters" do
      expect { subject_experiment.class.exclude }.to raise_error(
        ArgumentError,
        'no filters provided'
      )
    end
  end

  describe ".segment" do
    it "adds the filters to the correct callback chain" do
      subject_experiment.class.segment(variant: '_bar_') { false }
      subject_experiment.class.segment(variant: '_foo_') { true }
      subject_experiment.class.segment(variant: '_baz_') { true } # this will never get called because we terminate

      subject_experiment.run_callbacks(:segmentation)

      expect(subject_experiment.assigned.name).to eq('_foo_')
    end

    it "raises an exception when there are no filters" do
      expect { subject_experiment.class.segment(variant: :variant) }.to raise_error(
        ArgumentError,
        'no filters provided'
      )
    end
  end

  describe "run callbacks" do
    it "supports before, after, and around style callbacks" do
      result = []
      subject_experiment.class.before_run { result << 'before1' }
      subject_experiment.class.after_run { result << 'after1' }
      subject_experiment.class.around_run do |_e, b|
        result << 'around1['
        b.call
        result << ']'
      end

      subject_experiment.class.before_run { result << 'before2' }
      subject_experiment.class.after_run { result << 'after2' }
      subject_experiment.class.around_run do |_e, b|
        result << 'around2['
        b.call
        result << ']'
      end

      subject_experiment.run_callbacks(:run)

      expect(result.join('_')).to eq('before1_around1[_before2_around2[_]_after2_]_after1')
    end
  end
end
