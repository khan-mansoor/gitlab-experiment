# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Context do
  subject { described_class.new(subject_experiment) }

  let(:subject_experiment) { Gitlab::Experiment.new(:example) }
  let(:request) { double(headers: {}, cookie_jar: cookie_jar) }
  let(:cookie_jar) { double(signed: {}) }

  before do
    allow(subject_experiment).to receive(:variant).and_return(double(name: '_variant_'))
  end

  it "allows setting values multiple times" do
    subject.value(foo: 'bar')
    subject.value(bar: 'baz')

    expect(subject.value).to eq(foo: 'bar', bar: 'baz')
  end

  it "generates the signature when frozen", :suppress_warnings do
    expect(subject).to receive(:signature)

    subject.freeze
  end

  it "handles proxying missing methods to the value hash" do
    subject.value(foo: 'bar')

    expect(subject.foo).to eq('bar')
    expect(subject.send('foo')).to eq('bar')
    expect(subject).to respond_to('foo')

    expect { subject.bar }.to raise_error(NoMethodError)
    expect(subject).not_to respond_to('bar')
  end

  describe "sticky contexts" do
    let(:project) { double(id: 42, to_global_id: 'gid://glex/Project/42') }

    it "allows specifying what the context is sticky to" do
      subject.value(foo: 'bar', sticky_to: 'abc')
      expect(subject.key).to eq('abc')
    end

    it "can handle hashes" do
      subject.value(foo: 'bar', sticky_to: { bar: 'baz' })
      expect(subject.key).to eq(key_for(subject_experiment, bar: 'baz'))
    end

    it "can handle model like objects" do
      subject.value(foo: 'bar', sticky_to: project)
      expect(subject.key).to eq(key_for(subject_experiment, project))
    end

    it "can handle ids and similar" do
      subject.value(foo: 'bar', sticky_to: project.id)
      expect(subject.key).to eq(key_for(subject_experiment, project.id))
    end
  end

  describe "tracking" do
    it "is allowed when there's no request" do
      expect(subject).to be_trackable
    end

    it "is allowed when there's no DNT header" do
      subject.value(actor: 42, request: request)

      expect(subject).to be_trackable
    end

    it "isn't allowed when there's a DNT header" do
      subject.value(actor: 42, request: double(headers: { 'DNT' => 1 }, cookie_jar: cookie_jar))

      expect(subject).not_to be_trackable
    end
  end

  describe "keys and signatures" do
    let(:keys) do
      [
        key_for(subject_experiment, foo: 'bar', version: 0),
        key_for(subject_experiment, foo: 'bar', version: 1),
        key_for(subject_experiment, foo: 'bar', version: 2)
      ]
    end

    it "allows setting the key manually" do
      subject.key('abc123')

      expect(subject.key).to eq('abc123')
      expect(subject.signature).to eq(key: 'abc123')
    end

    it "generates a valid key for the value" do
      subject.value(foo: 'bar', version: 0)

      expect(subject.key).to eq(keys[0])
      expect(subject.signature).to eq(key: keys[0])
    end

    it "allows migrating using full contexts" do
      subject.value(foo: 'bar', version: 1, migrated_from: { foo: 'bar', version: 0 })

      expect(subject.signature).to eq(key: keys[1], migration_keys: keys[0..0])

      subject.value(foo: 'bar', version: 2, migrated_from: { foo: 'bar', version: 1 })

      expect(subject.signature).to eq(key: keys[2], migration_keys: keys[0..1])
    end

    it "allows migrating using partial contexts" do
      subject.value(foo: 'bar', version: 1, migrated_with: { version: 0 })

      expect(subject.signature).to eq(key: keys[1], migration_keys: keys[0..0])

      subject.value(foo: 'bar', version: 2, migrated_with: { version: 1 })

      expect(subject.signature).to eq(key: keys[2], migration_keys: keys[0..1])
    end
  end
end
