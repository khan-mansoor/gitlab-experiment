# frozen_string_literal: true

module ConfigHelpers
  def key_for(experiment_or_name, source = nil)
    experiment = experiment_from_name(experiment_or_name)
    experiment.key_for(source || experiment.context.value)
  end

  def experiment_from_name(experiment_or_name)
    return experiment_or_name if experiment_or_name.respond_to?(:context)

    Gitlab::Experiment.new(experiment_or_name.to_s.gsub("#{config.name_prefix}_", ''))
  end

  def config
    Gitlab::Experiment::Configuration
  end
end

RSpec.configure do |config|
  config.include ConfigHelpers

  store = Gitlab::Experiment::Cache::RedisHashStore.new(pool: ->(&block) { block.call(Redis.current) })
  config.around(:each, :cache) do |example|
    original = Gitlab::Experiment::Configuration.cache
    Gitlab::Experiment::Configuration.cache = store
    example.run
    Redis.current.flushall # sorry if you have stuff in here!
    Gitlab::Experiment::Configuration.cache = original
  end
end
